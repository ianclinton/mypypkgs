import sqlite3

class dbControl(object):
    """docstring for dbControl"""
    def __init__(self, db_file, row_factory=True):
        super(dbControl, self).__init__()
        self.__conn = sqlite3.connect(db_file)
        if row_factory is True:
            self.__conn.row_factory = sqlite3.Row
        self.__cursor = self.__conn.cursor()
        self.__cursor.execute('PRAGMA journal_mode = wal')
        self.__cursor.execute("PRAGMA foreign_keys = 1")
        self.__cursor.execute("PRAGMA auto_vacuum = 1")
        self.__cursor.execute("PRAGMA user_version = 1")
        self.params = []
    
    def addFunction(self, name, num, func):
        self.__conn.create_function(name, num, func)

    def createTable(self, table_name, table_data):
        # table_data is a dict column name is the key column data type is the value
        table_cols = ', '.join(f'{key} {value}' for key, value in table_data.items())
        self.__cursor.execute(f'CREATE TABLE IF NOT EXISTS {table_name} ({table_cols});')

    def insert(self, table_name, table_data):
        # table_data is a list of dicts column name is the key column data is the value
        table_data = [table_data] if not type(table_data) == list else table_data       
        table_cols = ", ".join(table_data[0])   
        placeholder = ":" + ", :".join(table_data[0])
        self.__cursor.executemany(f'INSERT OR IGNORE INTO {table_name} ({table_cols}) VALUES ({placeholder})', table_data)

    def upsert(self, table_name, table_data, conflict_key='id'):
        # table_data is a list of dicts column name is the key column data is the value
        table_data = [table_data] if not type(table_data) == list else table_data       
        table_cols = ", ".join(table_data[0])   
        placeholder = ":" + ", :".join(table_data[0])

        cols = []
        [cols.append(c) for c in table_cols.split(',')]
        s = ''
        for c in cols:
            if not c == conflict_key:
                if s == '':
                    s += c.strip() + ' = :' + c.strip() + ','
                else:
                    s += ' ' + c.strip() + ' = :' + c.strip() + ','
        s = s.rstrip(',')
        s1 = s.replace('=', '<>').replace(',', ' OR ')

        q = f'INSERT OR IGNORE INTO {table_name} ({table_cols}) VALUES ({placeholder}) ON CONFLICT({conflict_key}) DO UPDATE SET {s} WHERE {s1};'

        self.__cursor.executemany(q, table_data)

    def query(self, qry):
        try:
            qry = qry.format('WHERE 1 = 1{}')
            vals =[]
            row = ''
            # params is a list of dicts, keys are {logic, columnname, operator, value}
            for param in self.params:
                # when operator = IN value must be in a tuple
                if type(param['value']) == tuple:
                    placeholder = ','.join('?' * len(param['value']))
                    placeholder = "({})".format(placeholder)
                    for p in param['value']:
                        vals.append(p)
                # when operator = BETWEEN value must be in a list
                elif type(param['value']) == list:
                    placeholder = ' AND '.join('?' * len(param['value']))
                    placeholder = "{}".format(placeholder)
                    for p in param['value']:
                        vals.append(p)
                else:
                    placeholder = '?'
                    vals.append(param['value'])
                row += " {} {} {} {}".format(param['logic'], param['column'], param['operator'], placeholder)
            qry = qry.format(row) + ';'
            return [dict (row) for row in self.__cursor.execute(qry, vals).fetchall()]          
        except Exception as e:
            raise e
        finally:
            self.flushParams()

    def readScript(self, path):
        with open(path, 'r') as f:
            data = f.read()
            return data
    
    def execScript(self, path):
        with open(path, 'r') as f:
            data = f.read()
            self.__cursor.executescript(data)

    def exec(self, qry, data=None):
        if data is not None:
            self.__cursor.execute(qry, data)
        else:    
            self.__cursor.execute(qry)

    def addParam(self, col_name, col_value, operator='=', logic='AND'):
        self.params.append({'logic':logic, 'column':col_name,'operator':operator,'value':col_value})

    def flushParams(self):
        self.params = []

    def save(self):
        self.__conn.commit()

    def close(self):
        self.__cursor.close()
        self.__conn.close()

# if __name__ == '__main__':
#   try:
#       db = dbControl(':memory:')
#       db.createTable("Employee", {'id':'integer primary key autoincrement','first':'text', 'last':'text', 'pay':'integer'})
    
#       employee_data = [{'first':'joe', 'last':'bloggs', 'pay':25000},
#       {'first':'jan', 'last':'bloggs', 'pay':30000},
#       {'first':'john', 'last':'doe', 'pay':100000},
#       {'first':'jane', 'last':'doe', 'pay':75000}]
#       db.insert('Employee', employee_data)

#       db.addParam('first', ('joe', 'jan', 'jane'), 'IN')
#       db.addParam('pay', [30000,80000], 'BETWEEN')

#       q = db.query("SELECT * FROM Employee {} ORDER BY pay DESC")
#       for i in q:
#           print(i)

#       db.save()
#   except Exception as e:
#       raise e
#   finally:
#       db.close()
#       pass
