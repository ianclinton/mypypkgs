def left(string, value):
	left_of = string.find(value) if string is not None else ''
	return string[ : left_of].strip() if left_of > -1 else None

def right(string, value):
	right_of = string.find(value) if string is not None else ''
	return string[right_of + len(value) : ].strip() if right_of > -1 else None

def mid(string, value1, value2):
	if string is None:
		return None
	s = right(string, value1)
	if s is None:
		return None
	return left(s, value2)